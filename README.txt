Book Child Order
================

This module provides an extra tab on every node that is part of a book. This
tab allows users with the right permissions to change the order of the book
node's child nodes.
